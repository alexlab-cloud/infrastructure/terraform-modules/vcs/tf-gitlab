# terraform-gitlab

[![Open in Dev Containers](https://img.shields.io/static/v1?label=Dev%20Containers&style=for-the-badge&message=Open&color=blue&logo=visualstudiocode)](https://vscode.dev/redirect?url=vscode://ms-vscode-remote.remote-containers/cloneInVolume?url=https://gitlab.com/alexlab-cloud/infrastructure/terraform-gitlab)

Basic management and automation of GitLab repository infrastructure using [Terraform](https://www.terraform.io/).

## Usage

### Dev Container [Optional]

This project's `devcontainer.json` defines extensions, installs dependencies, and changes editor settings to be ready for development on Visual
Studio Code.

Using Visual Studio Code with the Dev Containers extension installed, use `F1` to bring up the Command Palette and run `Dev Containers:
Rebuild And Run In A Container` to open the project in the development container. Alternatively, just use the button at the top of this README to do
everything automatically.

### Steps

1. Sign into [Terraform Cloud](https://app.terraform.io/app) to provide secrets and other variables to the app
2. `terraform init`
3. `bash tf-plan.sh`
4. `bash tf-apply.sh`

## CI/CD

The [pipeline](./.gitlab-ci.yml) that ships with this repository only checks Terraform formatting and validation. Any
building/deployment happens externally, managed by an account on [Terraform Cloud](https://app.terraform.io/app) and invoked by the command line or
other methods.

## Resources

### Documentation

- [GitLab's Terraform provider docs](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs)
- [Dev Containers](https://code.visualstudio.com/docs/devcontainers/containers)
- [Dev Containers Visual Studio Code extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

### `devcontainer.json`

[This `devcontainer.json` file](https://github.com/devcontainers-contrib/templates/blob/main/src/terraform-basic/.devcontainer/devcontainer.json)
from [`devcontainers-contrib/templates`](https://github.com/devcontainers-contrib/templates/tree/main) was used as the starting point for
this project's [`devcontainer.json`](./.devcontainer/devcontainer.json)

---

Emoji used for repository logo designed by OpenMoji – the open-source emoji and icon project. License: CC BY-SA 4.0
