#!/bin/bash

. .env

terraform plan \
-var "gitlab_pat=$GITLAB_PAT" \
-var "new_project_name=$NEW_PROJECT_NAME" \
-var "new_project_path=$NEW_PROJECT_PATH" \
-var "new_project_description=$NEW_PROJECT_DESCRIPTION" \
-var "ops_account_email=$OPS_ACCOUNT_EMAIL" \
-var "ops_account_name=$OPS_ACCOUNT_NAME"

