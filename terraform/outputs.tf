# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project

output "gitlab_repo_url" {
    value = new_project.web_url
}
