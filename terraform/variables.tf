variable "gitlab_pat" {
  description = "GitLab Personal Access Token for using API resources."
  type        = string
}

variable "new_project_name" {
  type = string
}

variable "new_project_path" {
  type = string
}

variable "new_project_description" {
  type = string
}

variable "ops_account_email" {
  description = "Email address for a bot/operations account."
  type        = string
}

variable "ops_account_name" {
  description = "Name for an bot/operations account."
  type        = string
}
